﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace lab
{
	public class EnvPicker : MonoBehaviour
	{

		// Use this for initialization
		void Start()
		{
			var ge = new GameEnvironment();
			ge.ForceInit();

			GetComponent<Text>().text = ge.Environment.ToString();
		}

		void Update()
		{
			if (Input.GetKeyUp(KeyCode.Escape))
				Application.Quit();
		}

	}
}