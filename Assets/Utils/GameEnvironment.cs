﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace lab
{
	public enum EnvironmentType
	{
		Dev,
		Stage,
		Prod
	}

	[Serializable]
	public class GameEnvironmentModel
	{
		public string Version { get; set; }

		public EnvironmentType Environment { get; set; }
	}

	public class GameEnvironment
	{
		LocalStorage localStorage;

		private const string serverBase = "http://{0}app{1}.appspot.com/api/";

		private const string versionBase = "{0}-dot-";

		private const string PrefsKeyVersion = "PrefsKeyVersion";
		private const string PrefsKeyEnvironment = "PrefsKeyEnvironment";
		private const string PrefsKeyServerDebugUrl = "PrefsKeyServerDebugUrl";

		private GameEnvironmentModel data;

		private static Dictionary<EnvironmentType, string> envBackendNames = new Dictionary<EnvironmentType, string>
		{
			{ EnvironmentType.Dev, "dev" },
			{ EnvironmentType.Stage, "test" },
			{ EnvironmentType.Prod, "backend" }
		};

		#region IInjectable implementation

		public void BindCompleted()
		{
			ForceInit();
		}

		#endregion

		public EnvironmentType Environment
		{
			set
			{
				data.Environment = value;
				Save();
				Debug.LogFormat("Environment is set to {0}. Url is {1}", data.Environment.ToString(), GetUrl());
			}
			get
			{
				return data.Environment;			
			}
		}

		public string Version
		{
			set
			{
				data.Version = value;
				Save();
				Debug.LogFormat("Version is set to {0}. Url is {1} ", data.Version, GetUrl());
			}
			get
			{
				return data.Version;
			}
		}

		public void ForceInit()
		{
			localStorage = new LocalStorage(Storage.ResourcesFolder);
			Load();
		}

		public void SetDebugUrl(string debugUrl)
		{
			PlayerPrefs.SetString(PrefsKeyServerDebugUrl, debugUrl);
		}

		public string GetUrl(bool useDebugUrl = false)
		{
			if (useDebugUrl && PlayerPrefs.HasKey(PrefsKeyServerDebugUrl))
				return PlayerPrefs.GetString(PrefsKeyServerDebugUrl);
			
			var versionPart = !string.IsNullOrEmpty(data.Version) 
				? string.Format(versionBase, data.Version) 
				: string.Empty; // Empty by default

			var envPart = envBackendNames[data.Environment];
			
			return string.Format(serverBase, versionPart, envPart);
		}

		private void Save()
		{
			localStorage.Save<GameEnvironmentModel>(data, enumsAsString: true);
			//AssetDatabase.Refresh();
		}

		private void Load()
		{
			if (localStorage.Has<GameEnvironmentModel>())
			{
				data = localStorage.Load<GameEnvironmentModel>();
				Debug.Log("Environment file is found, using " + data.Environment);
			}
			else
			{
				data = new GameEnvironmentModel();
				Debug.Log("Environment file is not found, using default value");
			}
		}
	}
}