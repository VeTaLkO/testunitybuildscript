﻿using UnityEngine;
using System;
using System.Collections;
using LitJson;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections.Generic;
using lab;

public enum Storage
{
	PersistentData,
	ResourcesFolder
}

public class LocalStorage
{
	private static Dictionary<Storage, string> storagePaths = new Dictionary<Storage, string>
	{
		{ Storage.PersistentData, Application.persistentDataPath },
		{ Storage.ResourcesFolder, Path.Combine(Application.dataPath, "Resources") }
	};

	private Storage storage;

	public LocalStorage()
		: this(Storage.PersistentData)
	{
	}

	public LocalStorage(Storage path)
	{
		storage = path;
	}

	public string StoragePath
	{
		get
		{  
			return storagePaths[storage];
		}
	}

	public void ClearAllData(IList<string> skip)
	{		
		Debug.Log("Clearing directory: " + StoragePath);

		string[] files = Directory.GetFiles(StoragePath);

		foreach (string file in files)
		{
			string fileWoExt = file.Substring(0, file.LastIndexOf("."));
			fileWoExt = fileWoExt.Substring(fileWoExt.LastIndexOf(Path.DirectorySeparatorChar) + 1);

			if (skip == null || !skip.Contains(fileWoExt))
				File.Delete(file);
		}
	}

	public void ClearData<T>(string fileName = null)
	{
		if (fileName == null)
			fileName = typeof(T).ToString().Split('.').Last();		

		string path = Path.Combine(StoragePath, fileName + ".txt");

		if (File.Exists(path))
			File.Delete(path);
	}

	public bool Has<T>(string fileName = null)
	{
		if (fileName == null)
			fileName = typeof(T).ToString().Split('.').Last();		


		if (storage == Storage.ResourcesFolder)
		{
			var asset = Resources.Load(fileName) as TextAsset;
			return asset != null;
		}
		if (storage == Storage.PersistentData)
		{
			string path = Path.Combine(StoragePath, fileName + ".txt");
			return File.Exists(path);
		}

		throw new IOException("File " + fileName + " not found");
	}

	public T Load<T>(string fileName = null)
	{
		if (fileName == null)
			fileName = typeof(T).ToString().Split('.').Last();		
		
		if (storage == Storage.ResourcesFolder)
		{
			var asset = Resources.Load(fileName) as TextAsset;
			Debug.Log("Reading from (Storage.ResourcesFolder) " + fileName + " : " + Environment.NewLine + asset.text);

			return PolyJsonMapper.ToObject<T>(asset.text);
		}
		if (storage == Storage.PersistentData)
		{
			string path = Path.Combine(StoragePath, fileName + ".txt");
			string json = File.ReadAllText(path, Encoding.UTF8);

			Debug.Log("Reading from (Storage.PersistentData) " + path + " : " + Environment.NewLine + json);

			return PolyJsonMapper.ToObject<T>(json);
		}

		throw new IOException("File " + fileName + " not found");
	}

	public void Save<T>(T obj, string fileName = null, bool isPrettyPrint = false, bool enumsAsString = false)
	{
		if (fileName == null)
			fileName = typeof(T).ToString().Split('.').Last();

		string path = Path.Combine(StoragePath, fileName + ".txt");

		StringBuilder sb = new StringBuilder();
		JsonWriter writer = new JsonWriter(sb);
		writer.PrettyPrint = isPrettyPrint;
		writer.EnumsAsString = enumsAsString;

		JsonMapper.ToJson(obj, writer);

		File.WriteAllText(path, sb.ToString(), Encoding.UTF8);

		Debug.Log("Writing to " + path + " : " + Environment.NewLine + sb.ToString());
	}
}

