﻿using System;

[AttributeUsage(AttributeTargets.Property)]
public class NonSerializedProperty : Attribute
{

}
