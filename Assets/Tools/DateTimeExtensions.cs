﻿using System;


public static class DateTimeExtensions
{

	static DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

	public static DateTime FromUnixTime(this long unixTime, bool useMillis)
	{
		if (useMillis)
			return epoch.AddMilliseconds(unixTime);
		else
			return epoch.AddSeconds(unixTime);
	}

	public static DateTime FromUnixTime(this int unixTime, bool useMillis)
	{
		if (useMillis)
			return epoch.AddMilliseconds(unixTime);
		else
			return epoch.AddSeconds(unixTime);
	}

	public static long ToUnixTime(this DateTime date, bool useMillis)
	{
		if (useMillis)
			return Convert.ToInt64((date.ToUniversalTime() - epoch).TotalMilliseconds);
		else
			return Convert.ToInt64((date.ToUniversalTime() - epoch).TotalSeconds);
	}
}
