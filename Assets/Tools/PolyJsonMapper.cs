﻿using System;
using LitJson;
using System.Collections.Generic;
using System.Collections;
using System.Reflection;

namespace lab
{
	public class PolyJsonMapper
	{
		public const string JsonPolyKey = "ClassName";

		public static T ToObject<T>(string json)
		{
			JsonData data = JsonMapper.ToObject(json);

			return (T)readRecursive(typeof(T), data);
		}

		public static T ToObject<T>(string json, bool fromJava)
		{
			JsonReader reader = new JsonReader(json);
			reader.FromJava = fromJava;

			JsonData data = JsonMapper.ToObject(reader);

			return (T)readRecursive(typeof(T), data);
		}

		// // // // //

		static BindingFlags bindingAttr = BindingFlags.Default | BindingFlags.Instance | BindingFlags.Public | BindingFlags.FlattenHierarchy;

		static object readRecursive(Type baseType, JsonData data)
		{
			if (data == null)
				return null;
			
			//Primitives
			switch (data.GetJsonType())
			{
				case JsonType.String:
					{
						if (baseType == typeof(DateTime))
							return Convert.ToDateTime((string)data);

						if (baseType.IsAssignableFrom(typeof(string)))
							return (string)data;
						return null;
					}

				case JsonType.Int:
					{
						if (baseType == typeof(DateTime))
							return ((int)data).FromUnixTime(true);
						
						if (baseType.IsAssignableFrom(typeof(int)))
							return (int)data;

						if (baseType.IsAssignableFrom(typeof(long)))
						{
							return Convert.ToInt64(data.ToString()); // NOTE: Temporary workaround for DataCurrency - data.GetJsonType() is Int somehow
						}
							

						if (baseType == typeof(double))
							return (double)(int)data;

						return null;
					}	

				case JsonType.Long:
					{
						if (baseType == typeof(DateTime))
							return ((long)data).FromUnixTime(true);

						if (baseType.IsAssignableFrom(typeof(long)))
							return (long)data;

						if (baseType == typeof(double))
							return (double)(long)data;

						return null;
					}

				case JsonType.Double:
					{
						if (baseType.IsAssignableFrom(typeof(double)))
							return (double)data;
						return null;
					}

				case JsonType.Boolean:
					{
						if (baseType.IsAssignableFrom(typeof(bool)))
							return (bool)data;
						return null;
					}
			}

			object instance = null;

			if (data.IsObject)
			{
				bool isDictionary = (baseType.GetInterface("System.Collections.IDictionary") != null);

				if (data.Keys.Contains(JsonPolyKey))
				{
					baseType = Type.GetType((string)data[JsonPolyKey]);
				}

				instance = Activator.CreateInstance(baseType);

				if (isDictionary)
				{
					instance = Activator.CreateInstance(baseType);


					if (baseType.GetGenericArguments().Length < 2)
						UnityEngine.Debug.LogWarning("type:  " + baseType);

					Type elementType = baseType.GetGenericArguments()[1];

					foreach (string key in data.Keys)
					{
						object item = readRecursive(elementType, data[key]);

						//if (item == null)
						//	continue;
						var typing = ((IDictionary)instance).GetType().GetGenericArguments();
						Type keyType = typing[0];

						if (keyType.IsEnum)
						{
							((IDictionary)instance).Add(Enum.Parse(keyType, key), item);
						}
						else
						{
							((IDictionary)instance).Add(key, item);
						}

					}
				}
				else
				{
					FieldInfo[] fields = instance.GetType().GetFields(bindingAttr);
					foreach (FieldInfo field in fields)
					{
						Type fieldType = field.FieldType;

						if (data.Keys.Contains(field.Name))
						{

							if (fieldType.IsEnum)
							{
								if (data[field.Name].IsInt)
									field.SetValue(instance, Enum.ToObject(fieldType, readRecursive(typeof(int), data[field.Name])));
								else
									field.SetValue(instance, Enum.Parse(fieldType, (string)readRecursive(typeof(string), data[field.Name])));								
							}
							else
							{
								object item = readRecursive(fieldType, data[field.Name]);

								if (item == null)
									continue;

								field.SetValue(instance, item);
							}
						}
					}

					PropertyInfo[] props = instance.GetType().GetProperties(bindingAttr);
					foreach (PropertyInfo prop in props)
					{
						if (!prop.CanWrite)
							continue;

						Type fieldType = prop.PropertyType;

						if (data.Keys.Contains(prop.Name))
						{
							if (fieldType.IsEnum)
							{
								if (data[prop.Name].IsInt)
									prop.SetValue(instance, Enum.ToObject(fieldType, readRecursive(typeof(int), data[prop.Name])), null);
								else
									prop.SetValue(instance, Enum.Parse(fieldType, (string)readRecursive(typeof(string), data[prop.Name])), null);
							}
							else
							{
								object item = readRecursive(fieldType, data[prop.Name]);

								if (item == null)
									continue;
								
								prop.SetValue(instance, item, null);
							}
						}
					}
				}
			}

			if (data.IsArray)
			{
				int len = data.Count;

				Type elementType = null;

				if (baseType.IsArray)
				{
					elementType = baseType.GetElementType();

					instance = Array.CreateInstance(elementType, len);

					for (int i = 0; i < len; i++)
						((Array)instance).SetValue(readRecursive(elementType, data[i]), i);
				}
				else
				{
					elementType = baseType.GetGenericArguments()[0];

					instance = (IList)Activator.CreateInstance(baseType);

					for (int i = 0; i < len; i++)
					{
						if (elementType.IsEnum)
						{
							var res = Enum.Parse(elementType, data[i].ToString());
							((IList)instance).Add(res);
						}
						else
						{
							var res = data[i];
							((IList)instance).Add(readRecursive(elementType, res));
						}

					}
						
				}
			}

			return instance;
		}
	}
}
