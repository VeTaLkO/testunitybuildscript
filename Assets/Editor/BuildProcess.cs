﻿using System;
using System.Text;
using UnityEngine;
using System.Collections.Generic;
using LitJson;
using UnityEditor;
using UnityEditor.Callbacks;
using System.IO;

#if UNITY_IOS
using UnityEditor.iOS.Xcode;
#endif

namespace lab
{
	public class BuildProcess
	{
		[MenuItem("LAB/Action/TestBuild")]
		public static void PostBuildEditor()
		{
			PerformTestBuild();
		}

		[MenuItem("LAB/Action/DevBuild")]
		public static void PostBuildEditor1()
		{
			PerformDevBuild();
		}

		[MenuItem("LAB/Action/StageBuild")]
		public static void PostBuildEditor2()
		{
			PerformStageBuild();
		}

		[MenuItem("LAB/Action/ProdBuild")]
		public static void PostBuildEditor3()
		{
			PerformProdBuild();
		}

		[MenuItem("LAB/Action/AllBuild")]
		public static void PostBuildEditor6()
		{
			PerformTestBuild();
			PerformDevBuild();
			PerformStageBuild();
			PerformProdBuild();
		}

		/*

		#if UNITY_IOS
		
		[PostProcessBuild]
		public static void OnPostprocessBuild(BuildTarget buildTarget, string path)
		{
			if (buildTarget == BuildTarget.iOS)
			{
				string projPath = path + "/Unity-iPhone.xcodeproj/project.pbxproj";
				PBXProject proj = new PBXProject();
				proj.ReadFromString(File.ReadAllText(projPath));

				string nativeTarget = proj.TargetGuidByName(PBXProject.GetUnityTargetName());
				string testTarget = proj.TargetGuidByName(PBXProject.GetUnityTestTargetName());
				string[] buildTargets = new string[]{ nativeTarget, testTarget };

				proj.SetBuildProperty(buildTargets, "ENABLE_BITCODE", "NO");
				File.WriteAllText(projPath, proj.WriteToString());
			}
		}


		#endif
*/


		// Triggered by CloudBuild
		public static void PreBuild()
		{
			Debug.Log("lab.BuildProcess.PreBuild");

			SetEnvByCloud();
		}

		// Triggered by Editor
		public static void PerformTestBuild()
		{
			Debug.Log("PerformTestBuild");

			string currentDir = Path.GetDirectoryName(Application.dataPath);
			var buildFolder = Directory.CreateDirectory(Path.Combine(currentDir, "Builds/Test"));

			var gameEnvironment = new GameEnvironment();
			gameEnvironment.ForceInit();
			gameEnvironment.Environment = EnvironmentType.Prod;

			AssetDatabase.Refresh();

			string[] scenes = { "Assets/Scenes/TestScene.unity" };
			BuildPipeline.BuildPlayer(scenes, buildFolder.FullName, BuildTarget.StandaloneOSXUniversal, BuildOptions.None);
		}

		// Triggered by buildLocal.sh script
		public static void PerformLocalBuild()
		{		
			Debug.Log("PerformLocalBuild");

			string currentDir = Path.GetDirectoryName(Application.dataPath);
			var buildFolder = Directory.CreateDirectory(Path.Combine(currentDir, "../../Builds/Local/iOS"));

			AssetDatabase.Refresh();

			string[] scenes = { "Assets/Scenes/Game.unity" };
			BuildPipeline.BuildPlayer(scenes, buildFolder.FullName, BuildTarget.iOS, BuildOptions.None);
		}


		// Triggered by buildDev.sh script
		public static void PerformDevBuild()
		{
			Debug.Log("PerformDevBuild");

			string currentDir = Path.GetDirectoryName(Application.dataPath);
			var buildFolder = Directory.CreateDirectory(Path.Combine(currentDir, "Builds/Dev"));

			var gameEnvironment = new GameEnvironment();
			gameEnvironment.ForceInit();
			gameEnvironment.Environment = EnvironmentType.Dev;

			AssetDatabase.Refresh();

			string[] scenes = { "Assets/Scenes/TestScene.unity" };
			BuildPipeline.BuildPlayer(scenes, buildFolder.FullName, BuildTarget.StandaloneOSXUniversal, BuildOptions.None);
		}

		// Triggered by buildStage.sh script
		public static void PerformStageBuild()
		{			
			Debug.Log("PerformStageBuild");

			string currentDir = Path.GetDirectoryName(Application.dataPath);
			var buildFolder = Directory.CreateDirectory(Path.Combine(currentDir, "Builds/Stage"));

			var gameEnvironment = new GameEnvironment();
			gameEnvironment.ForceInit();
			gameEnvironment.Environment = EnvironmentType.Stage;

			AssetDatabase.Refresh();

			string[] scenes = { "Assets/Scenes/TestScene.unity" };
			BuildPipeline.BuildPlayer(scenes, buildFolder.FullName, BuildTarget.StandaloneOSXUniversal, BuildOptions.None);

			// Android
			//var buildFolderAndroid = Directory.CreateDirectory(Path.Combine(currentDir, "../../Builds/Stage/Android"));

			//BuildPipeline.BuildPlayer(scenes, buildFolderAndroid.FullName, BuildTarget.Android, BuildOptions.None);
		}

		// Triggered by buildProd.sh script
		public static void PerformProdBuild()
		{			

			Debug.Log("PerformProdBuild");

			string currentDir = Path.GetDirectoryName(Application.dataPath);
			var buildFolder = Directory.CreateDirectory(Path.Combine(currentDir, "Builds/Prod"));

			var gameEnvironment = new GameEnvironment();
			gameEnvironment.ForceInit();
			gameEnvironment.Environment = EnvironmentType.Prod;

			AssetDatabase.Refresh();

			string[] scenes = { "Assets/Scenes/TestScene.unity" };
			BuildPipeline.BuildPlayer(scenes, buildFolder.FullName, BuildTarget.StandaloneOSXUniversal, BuildOptions.None);
		}


		public static void PostBuild(string path = null)
		{
			Debug.Log("lab.BuildProcess.PostBuild");

			//SetEnvByCloud();
//			curl -d '{"color":"green","message":" (yey)","notify":false,"message_format":"text"}' -H 'Content-Type: application/json' https://labgames.hipchat.com/v2/room/1909655/notification?auth_token=wJ0ejy6wkgMxblxbchCTa3UwHJpz09rwg8iZx3NW

			/*
			string url = "https://labgames.hipchat.com/v2/room/1909655/notification?auth_token=wJ0ejy6wkgMxblxbchCTa3UwHJpz09rwg8iZx3NW";
			StringBuilder buildInfoStr = new StringBuilder();
			var buildInfo = GetBuildInfo();

			if (buildInfo != null)
			{
				buildInfoStr.Append("<b><a href=\"https://build.cloud.unity3d.com/orgs/lab-games/projects/sportsbillionaire/\">");
				buildInfoStr.Append("Build: #" + buildInfo["buildNumber"]);
				buildInfoStr.Append("</a></b>");

				buildInfoStr.Append("<br/>  Target: " + buildInfo["cloudBuildTargetName"]);
				buildInfoStr.Append("<br/>  BundleId: " + buildInfo["bundleId"]);
			}
			else
			{
				buildInfoStr.Append("(Missing UnityCloudBuildManifest) ");
			}

			buildInfoStr.Append("<br/><i>(Available soonish)</i>");


			var json = new Dictionary<string, object>();
			json.Add("color", "purple");
			json.Add("message", buildInfoStr.ToString());
			json.Add("notify", false);
			json.Add("message_format", "html");

			string jsonData = JsonMapper.ToJson(json);

			var headers = new Dictionary<string, string>();
			headers.Add("Content-Type", "application/json");

			Debug.Log(jsonData);

			var www = new WWW(url, Encoding.UTF8.GetBytes(jsonData), headers);

			ContinuationManager.Add(() => www.isDone, () =>
				{
					if (!string.IsNullOrEmpty(www.error))
						Debug.Log("WWW failed: " + www.error);
					
					Debug.Log("Retrieved: " + www.text);		
				}
			);
			*/
		}


		static Dictionary<string,object> GetBuildInfo()
		{
			var manifest = (TextAsset)Resources.Load("UnityCloudBuildManifest.json");
			if (manifest != null)
			{
				Dictionary<string,object> manifestDict = JsonMapper.ToObject<Dictionary<string,object>>(manifest.text);
				return manifestDict;
				/*
				Debug.Log("UnityCloudBuildManifest:");
				foreach (var kvp in manifestDict)
				{
					// Be sure to check for null values!
					var value = (kvp.Value != null) ? kvp.Value.ToString() : "";
					Debug.Log(string.Format(" {0} : {1}", kvp.Key, value));
				}
				*/
			}
			return null;
		}

		static void SetEnvByCloud()
		{
			#if CLOUD_DEV
			var gameEnvironment = new GameEnvironment();
			gameEnvironment.ForceInit();
			gameEnvironment.Environment = EnvironmentType.Dev;
			#elif CLOUD_STAGE
			var gameEnvironment = new GameEnvironment();
			gameEnvironment.ForceInit();
			gameEnvironment.Environment = EnvironmentType.Stage;
			#elif CLOUD_PROD
			var gameEnvironment = new GameEnvironment();
			gameEnvironment.ForceInit();
			gameEnvironment.Environment = EnvironmentType.Stage;
			#endif
		}





		public static void CloudPreBuildDev()
		{
			Debug.Log("CloudPreBuildDev");

			//string currentDir = Path.GetDirectoryName(Application.dataPath);
			//var buildFolder = Directory.CreateDirectory(Path.Combine(currentDir, "Builds/Dev"));

			var gameEnvironment = new GameEnvironment();
			gameEnvironment.ForceInit();
			gameEnvironment.Environment = EnvironmentType.Dev;

			AssetDatabase.Refresh();

			//string[] scenes = { "Assets/Scenes/TestScene.unity" };
			//BuildPipeline.BuildPlayer(scenes, buildFolder.FullName, BuildTarget.StandaloneOSXUniversal, BuildOptions.None);
		}

		// Triggered by buildStage.sh script
		public static void CloudPreBuildStage()
		{			
			Debug.Log("CloudPreBuildStage");

			//string currentDir = Path.GetDirectoryName(Application.dataPath);
			//var buildFolder = Directory.CreateDirectory(Path.Combine(currentDir, "Builds/Stage"));

			var gameEnvironment = new GameEnvironment();
			gameEnvironment.ForceInit();
			gameEnvironment.Environment = EnvironmentType.Stage;

			AssetDatabase.Refresh();

			//string[] scenes = { "Assets/Scenes/TestScene.unity" };
			//BuildPipeline.BuildPlayer(scenes, buildFolder.FullName, BuildTarget.StandaloneOSXUniversal, BuildOptions.None);

			// Android
			//var buildFolderAndroid = Directory.CreateDirectory(Path.Combine(currentDir, "../../Builds/Stage/Android"));

			//BuildPipeline.BuildPlayer(scenes, buildFolderAndroid.FullName, BuildTarget.Android, BuildOptions.None);
		}

	}
}

